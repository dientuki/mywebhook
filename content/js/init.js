$(document).ready(function() {

  MC.sticky.init();
  
  $('img.lzl').lazyload();
  
  MC.go.top();
  
  MC.go.anchor();
  
  MC.banner_slider();  
  
  MC.gallery();
  
  MC.form();
  
  $('html').addClass('js-finished').removeClass('no-js');
});
