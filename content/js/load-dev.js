Modernizr.load([
  {
    load: [MC.path + 'js/libs/jquery-1-10-2-min.js',
           MC.path + 'js/libs/marroncelli-namespace.js',
           MC.path + 'js/plugins/jquery-lazyload.js',
           MC.path + 'js/plugins/jquery-jcarrousellite.js',
           MC.path + 'js/init.js']
  }
]);
