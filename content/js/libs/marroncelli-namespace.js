MC.sticky = {};
MC.go = {};

MC.sticky.init = function(sidebar){
  // Validate IE6 just in case
  if (navigator.appName == 'Microsoft Internet Explorer'){
    if (navigator.userAgent.match(/ie/gi)) {
      var re = new RegExp("MSIE*.([0-9]{1,}[\.0-9]{0,})");
      if (re.exec(navigator.userAgent) != null){
        if (parseFloat( (RegExp.$1).replace('.',',') ) < 7){
          return false;
        }            
      }    
    } else {
      return false;
    }    
  }
  
  MC.sticky.np_top = $('#navigation-products').offset().top;
  MC.sticky.$body = $('body');
  MC.sticky.is_scrolling = true;
  
  setInterval(function () {
    MC.sticky.is_scrolling = true;
  }, 25);
  
  MC.sticky.run();
}

MC.sticky.run = function(){
  $(window).on('scroll', function(){
    if (MC.sticky.is_scrolling === true) {
      MC.sticky.is_scrolling = false;
      //now run your code to animate with respect to scroll
      
      var offset = window.pageYOffset;
      if (offset == undefined) {
          offset = document.documentElement.scrollTop;
      } 
      
      if (offset >= MC.sticky.np_top) {
        
        if (MC.sticky.$body.hasClass('sticky') == false ){
          MC.sticky.$body.addClass('sticky')
        }
      } else {
        
        if (MC.sticky.$body.hasClass('sticky') == true ){
          MC.sticky.$body.removeClass('sticky')
        }       
        
      }
    }        
  }).on('resize', function(){
    $(window).trigger('scroll');
  }).trigger('scroll');
}

MC.go.top = function(){
  $('#gotop').on('click',function(e){
  e.preventDefault();
    $('html, body').animate({ scrollTop: 0 });
  }); 
}

MC.go.anchor = function(){
  $('#main-content').find('.anchor-name').on('click', function(e){
    e.preventDefault();
    
    var gotu = 0,
        $block = $(this).closest('.block'),
        delta = $('#navigation-products').outerHeight(true),
        wh = window.innerHeight,
        hash = $(this).attr('href');
    
    if ($block.height() > (wh-delta)){
    //small screen
    
      gotu = $block.offset().top - delta; 
    } else {
    //big screen
    //gotu = $block.offset().top + delta + ((wh - delta)/2);
    
    gotu = $block.offset().top - delta - ((wh - delta - $block.height())/2);
    }
    
    
    
    $('html, body').animate({ scrollTop: gotu }, function(){
      //window.location.hash = hash;
    });
  });
}

MC.gallery = function(){
  if ($('#galeria').length == 1){
    
    $('#galeria').jCarrouselLite({
          btnPrev: '.arrow-prev',
          btnNext: '.arrow-next',   
          wrapper: '.gallery-wrapper',
          element: '.gallery-item',
          circular: false,
          visible: 1,
          afterEnd: function(){
            
            $(window).trigger('scroll');
          }
    });
  }
};

MC.banner_slider = function(){
  if ($('#banner').length == 1){
    
    $('#banner').jCarrouselLite({
          wrapper: '.gallery-wrapper',
          element: '.gallery-item',
          auto:4000,
          speed:750,
          visible: 1,
          afterEnd: function(){
            
            $('#banner').find('img').each(function(){
              $(this).attr('src', $(this).data('original')).removeClass('lzl')
            });
          }
    });
  }
};

MC.form = function(){
  if ($('#contact-form').length == 1){
    
    $('#contact-form').on('submit', function(e){
      $('.loading').css({'display':"block"});
      e.preventDefault();

      var jqxhr = $.ajax({type: "POST",
                          url: $(this).attr('action'),
                          data: $(this).serialize(),
                          dataType: "html",
                          async: true,
                          cache: true
                        });   
      
      jqxhr.success(function(data) {
        $('#contact-form').slideUp('fast');
        $('#contact-sucess').slideDown('slow');
        $('.loading').css({'display':"none"});
      });

      jqxhr.error(function(xhr, ajaxOptions, thrownError) {}); 
    })
  
  }
}