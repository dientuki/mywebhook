# All files in the 'lib' directory will be loaded
# before nanoc starts compiling.
require "i18n"

I18n.enforce_available_locales = false

def get_institutional
  menu = Array.new
  
  menu << {:href=>'index.html', :title=>'Home'}
  menu << {:href=>'empresa.html', :title=>'Empresa'}
  menu << {:href=>'fabricacion.html', :title=>'Fabricación'}
  menu << {:href=>'producto.html', :title=>'Servicios'}
  menu << {:href=>'galeria.html', :title=>'Galería'}        
  menu << {:href=>'folletos.html', :title=>'Folletos'}
  
  return menu          
  
end

def get_products
  menu = Array.new
  
  menu << {:href=>'puertas-acceso.html', :title=>'Puertas de acceso'}
  menu << {:href=>'puertas-acceso.html', :title=>'Puertas de interior'}
  menu << {:href=>'ventanas.html', :title=>'Ventanas'}
  menu << {:href=>'producto.html', :title=>'Accesorios'}
  menu << {:href=>'contacto.html', :title=>'Contacto', :css=>'envelope'}        
  
  return menu          
  
end

include Nanoc3::Helpers::Rendering
include Nanoc::Helpers::LinkTo
