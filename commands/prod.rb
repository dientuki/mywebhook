usage       'prod [options]'
summary     'compile for production'
description 'compile the proyect for test and send to system'

flag   :h, :help,  'show help for this awesome command' do |value, cmd|
  puts cmd.help
  exit 0
end

run do |opts, args, cmd|

  FileUtils::rm_rf 'nanoc.yaml'
  FileUtils::rm_rf 'output'
  FileUtils::rm_rf 'tmp'
  FileUtils::copy_entry 'nanoc-production.yaml', 'nanoc.yaml'  
  
  system 'nanoc compile'
  system 'sh compress.sh'  

  FileUtils::rm_rf 'nanoc.yaml'
  FileUtils::copy_entry 'nanoc-development.yaml', 'nanoc.yaml'
  FileUtils::copy_entry 'readme.txt', 'output/readme.txt'
    
  puts "Files compiled :)"
  
end
