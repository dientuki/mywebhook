echo 'starting to compress this shit...'

mkdir -p tmp/css

#compress css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o tmp/css/00.css  output/css/normalize.css
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o tmp/css/10.css  output/css/style.css


#concat
cat tmp/css/* > output/css/marroncelli-min.css


#compress ie shit
java -jar /usr/share/yui/yuicompressor.jar --type css --line-break 200 -o output/css/ie-min.css  output/css/ie.css


echo 'Cleaning console.log from this files'
grep -rl "console\." output/js/ --exclude="*-min.js"
grep -rl "console\." output/js/ --exclude="*-min.js" | xargs sed -rsi "s/console\.\w*\(.*\);?//g"

echo 'compressing, just wait a fucking moment... '
mkdir -p tmp/js

#compress js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o tmp/js/00.js output/js/libs/marroncelli-namespace.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o tmp/js/10.js output/js/plugins/jquery-lazyload.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o tmp/js/20.js output/js/plugins/jquery-jcarrousellite.js
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o tmp/js/99.js output/js/init.js

#concat js
cat tmp/js/* > output/js/marroncelli-min.js

#compress js without concat
java -jar /usr/share/yui/yuicompressor.jar --type js --line-break 200 -o output/js/load-min.js output/js/load-prod.js

#delete the tmp folder
rm -rf tmp

#delete the .scss files
find output/ -iname "*.scss" -exec rm {} \;

echo 'Ta dan!, Enjoy!'
